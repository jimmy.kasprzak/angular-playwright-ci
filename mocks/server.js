const express = require("express");
const cors = require("cors");
const app = express();

const port = 8081;

app.use(cors());

app.get("/rest/healthcheck", function (req, res, next) {
  res.send(200);
});

app.get("/rest/user", function (req, res, next) {
  res.json({ id: 0, firstName: "Riri", lastName: "Duck" });
});

app.listen(port, function () {
  console.log(`CORS-enabled web server listening on port ${port}`);
});
